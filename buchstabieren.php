<?php
/*
To the extent possible under law, Kevin Koopmann has waived all copyright and related or neighboring rights to this work. This work is published from Germany.
*/
	$buchstaben = array(	array("A", "Anton"),
							array("B", "Berta"),
							array("C", "Caesar"),
							array("D", "Dora"),
							array("E", "Emil"),
							array("F", "Friedrich"),
							array("G", "Gustav"),
							array("H", "Heinrich"),
							array("I", "Ida"),
							array("J", "Julius"),
							array("K", "Konrad"),
							array("L", "Ludwig"),
							array("M", "Martha"),
							array("N", "Nordpol"),
							array("O", "Otto"),
							array("P", "Paula"),
							array("Q", "Quelle"),
							array("R", "Richard"),
							array("S", "Siegfried"),
							array("T", "Theodor"),
							array("U", "Ulrich"),
							array("V", "Viktor"),
							array("W", "Wilhelm"),
							array("X", "Xanthippe"),
							array("Y", "Ypsilon"),
							array("Z", "Zeppelin"),
							array("0", "Null"),
							array("1", "Eins"),
							array("2", "Zwei"),
							array("3", "Drei"),
							array("4", "Vier"),
							array("5", "Fünf"),
							array("6", "Sechs"),
							array("7", "Sieben"),
							array("8", "Acht"),
							array("9", "Neun"),
							array(".", "Punkt"),
							array(":", "Doppelpunkt"),
							array(";", "Semikolon"),
							array(",", "Komma"),
							array("_", "Unterstrich"),
							array("ß", "Eszett"),
							array(" ", "Leerzeichen")
						);
					
	$fullstring = $_GET['string'];
	
	$chararray = str_split($fullstring); // Array mit Einzelbuchstaben als Items
	$chararray_length = count($chararray);
	
	foreach ($chararray as $key => $value) {	// Durchlauf des Arrays mit den Buchstaben des Strings für jedes Element
		$char = $chararray[$key];
		$char_search = strtoupper($char); // Suche mit Großbuchstaben
		
		foreach ($buchstaben as $bkey => $buchstabe) { // Durchlauf des Arrays mit den Zuordnungen; "bkey", weil bei Überschreiben von "key" Probleme ab 62
			if ($buchstaben[$bkey][0] == $char_search) { // Prüfung, ob der gegenwärtig geprüfte Buchstabe dem gesuchten aus dem Wort entspricht
				$success = TRUE;
				if (ctype_lower($char)) { // Prüft, ob Buchstabe klein ist
					$wort = strtolower($buchstaben[$bkey][1]);
				} else {
					$wort = $buchstaben[$bkey][1];
				}
				break;
			}
		}
		if ($success == FALSE) {
			$wort = "?";
			$unbekannte = 1;
		}
		if ($key < $chararray_length - 1) { // Prüfen, ob letztes Wort erreicht ist
			$finalstring = $finalstring . $wort . "-";
		} else {
			$finalstring = $finalstring . $wort;
		} 
		$success = FALSE;
	}
	
	if (!$unbekannte) {
		$unbekannte = 0;
	}
	
	echo '{"string": "' . $finalstring . '", "unbekannte": ' . $unbekannte . '}'; // Ausgabe als JSON
?>