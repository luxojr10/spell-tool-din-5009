/*
To the extent possible under law, Kevin Koopmann has waived all copyright and related or neighboring rights to this work. This work is published from Germany.
*/
$(function() {
	$("#wort").first().focus(); // Fokus beim Laden der Seite autom. auf Feld setzen
});
$("#wort").on('input', function() {
	$.ajax({
		url: "./buchstabieren.php",
		type: "GET",
		data: {string: $("#wort").val()},
		dataType: "json"
	}).done(function(response) {
		console.log(response);
		$("#result").html(response.string);
		
		if(response.unbekannte == 1) {
			$("#unbekannte").show();
		} else {
			$("#unbekannte").hide();
		}
	}).fail(function(msg, status) {
		alert("Fehler: " + status);
	});
});