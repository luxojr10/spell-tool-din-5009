<!--
To the extent possible under law, Kevin Koopmann has waived all copyright and related or neighboring rights to this work. This work is published from Germany.
-->
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="../commons/css/bootstrap.min.css"></style>
	</head>
	<body>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="page-header">
					<h1>Wort schreibgerecht diktieren <small>nach DIN 5009</small></h1>
				</div>
				<div class="well">
					<form role="form">
						<div class="form-group">
							<label for="exampleInputEmail1">Wort zum Diktieren</label>
							<input type="text" class="form-control" id="wort" placeholder="Wort eingeben">
						</div>
					</form>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Resultat</h3>
						</div>
						<div class="panel-body" id="result">
							Tippen, um anzufangen...
						</div>
					</div>

				</div>
				<div class="alert alert-danger" style="display: none" id="unbekannte"><strong>Hinweis:</strong> Der eingegebene Text enth&auml;lt Buchstaben, die nicht der Diktiertabelle nach DIN 5009 zugeordnet werden k&ouml;nnen. M&ouml;glicherweise sind die Resultate nicht wie erwartet.</div>
				<footer>
					  <small><a rel="license"
						 href="http://creativecommons.org/publicdomain/zero/1.0/">
						<img src="http://i.creativecommons.org/p/zero/1.0/80x15.png" style="border-style: none;" alt="CC0" />
					  </a><br />
					  To the extent possible under law, Kevin Koopmann
					  has waived all copyright and related or neighboring rights to
					  this work. This work is published from Germany.</small><br />
					<small><a href="https://bitbucket.org/luxojr10/spell-tool-din-5009/src">Access source on Bitbucket</a></small>
				</footer>
			</div>
			<div class="col-md-3"></div>
		</div>		
		<script type="text/javascript" src="../commons/js/jquery-2.0.3.min.js"></script>
		<script type="text/javascript" src="./main.js"></script>
	</body>
</html>